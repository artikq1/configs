#!/bin/bash

#stress-ng --seq 0 --aggressive --maximize --metrics-brief --log-brief -t 30s
stress-ng --mq 0 -t 1m --times --perf --log-brief

stress-ng --matrix 0 -t 1m --metrics-brief --log-brief

# cpu - CPU intensive
# cpu-cache - stress CPU instruction and/or data caches
# device - raw device driver stressors
# io - generic input/output
# interrupt - high interrupt load generators
# filesystem - file system activity
# memory - stack, heap, memory mapping, shared memory stressors
# network - TCP/IP, UDP and UNIX domain socket stressors
# os - core kernel stressors
# pipe - pipe and UNIX socket stressors
# scheduler - force high levels of context switching
# security - Selinux stressor
# vm - Virtual Memory stressor (paging and memory)

stress-ng --cpu 4 --cpu-method matrixprod --metrics --timeout 1m

stress-ng --class cpu --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class cpu-cache --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class device  --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class io --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class interrupt --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class filesystem --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class memory --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class network --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class os --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class pipe --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --class scheduler --sequential 0 --timeout 1m --metrics-brief --log-brief

#stress-ng --class security --all 1 --timeout 60s --metrics-brief --log-brief

stress-ng --class vm --sequential 0 --timeout 1m --metrics-brief --log-brief

stress-ng --cpu 4 --io 4 --vm 1 --vm-bytes 1G --timeout 1m --metrics-brief
stress-ng --cpu 0 --cpu-method all -t 1m
stress-ng --cacheline 32 -t 1m
stress-ng --iomix 1 -t 1m -v
stress-ng --cpu 2 --vm 4 -t 1m
stress-ng --shm 0 -t 1m
stress-ng --userfaultfd 0 --perf -t 1m
stress-ng --brk 0 --stack 0 --bigheap 0 -t 1m
stress-ng --cyclic 1 --cyclic-dist 250 --cyclic-method clock_ns --cyclic-sleep 20000 --cyclic-policy rr --vm 4 -t 60 --log-brief
stress-ng --tree 1 --tree-method all -t 15s --metrics --log-brief
stress-ng --vm-method all --vm 1 -t 15s
stress-ng --cpu 4 --cpu-method all -t 10 --metrics --log-brief
stress-ng --vm 1 --vm-bytes 2G --verify -v -t 1m
stress-ng --cpu 0 --verify -t 1m

#2x4 8gb
stress-ng --vm 4 --vm-bytes 2G --mmap 4 --mmap-bytes 2G --page-in -t 1m
