:set guicursor+=n:hor20-Cursor/lCursor
:set number
:set relativenumber
:set autoindent
:set tabstop=4
:set shiftwidth=4
:set smarttab
:set softtabstop=4
:set mouse=a
:set guifont=JetBrains\ Mono:h10
:set clipboard+=unnamedplus
