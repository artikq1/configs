:set guicursor+=n:hor20-Cursor/lCursor
:set number
:set relativenumber
:set autoindent
:set tabstop=4
:set shiftwidth=4
:set smarttab
:set softtabstop=4
:set mouse=a
:set guifont=JetBrains\ Mono:h10
call plug#begin('~/.local/share/nvim/site/plugged')
Plug 'pearofducks/ansible-vim'
call plug#end()
