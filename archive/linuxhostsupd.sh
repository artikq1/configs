#!/bin/bash

# Linux hosts Installer for the Ultimate Hosts Blacklist
# Repo Url: https://github.com/Ultimate-Hosts-Blacklist/Ultimate.Hosts.Blacklist
# Copyright (c) 2020 Ultimate Hosts Blacklist - @Ultimate-Hosts-Blacklist
# Copyright (c) 2017, 2018, 2019, 2020 Mitchell Krog - @mitchellkrogza
# Copyright (c) 2017, 2018, 2019, 2020 Nissar Chababy - @funilrys

#
# root has to run the script
#
if [[ $(id -u -n) != "root" ]]
    then
    printf "You need to be root to do this!\nIf you have SUDO installed, then run this script with `sudo ${0}`"
    exit 1
fi

# Now download the new hosts file and put it into place
wget https://hosts.ubuntu101.co.za/hosts -O /tmp/hosts0
wget https://adaway.org/hosts.txt -O /tmp/hosts1
wget https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts -O /tmp/hosts2
wget https://winhelp2002.mvps.org/hosts.txt -O /tmp/hosts3
wget https://www.hostsfile.org/Downloads/hosts.txt -O /tmp/hosts4
wget https://raw.githubusercontent.com/lewisje/jansal/master/adblock/hosts -O /tmp/hosts5
wget https://someonewhocares.org/hosts/hosts -O /tmp/hosts6
wget https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Hosts/GoodbyeAds.txt -O /tmp/hosts7
wget https://www.github.developerdan.com/hosts/lists/ads-and-tracking-extended.txt -O /tmp/hosts8
wget https://mkb2091.github.io/blockconvert/output/hosts.txt -O /tmp/hosts9
wget https://urlhaus.abuse.ch/downloads/hostfile/ -O /tmp/hosts10
wget https://orca.pet/notonmyshift/hosts.txt -O /tmp/hosts11
wget https://raw.githubusercontent.com/anudeepND/blacklist/master/facebook.txt -O /tmp/hosts12
wget https://zerodot1.gitlab.io/CoinBlockerLists/hosts -O /tmp/hosts13
wget https://zerodot1.gitlab.io/CoinBlockerLists/hosts_browser -O /tmp/hosts14
wget https://zerodot1.gitlab.io/CoinBlockerLists/hosts_optional -O /tmp/hosts15
wget https://raw.githubusercontent.com/anudeepND/blacklist/master/CoinMiner.txt -O /tmp/hosts16
wget https://raw.githubusercontent.com/badmojr/1Hosts/master/Xtra/hosts.txt -O /tmp/hosts17
wget https://www.github.developerdan.com/hosts/lists/tracking-aggressive-extended.txt -O /tmp/hosts18
wget https://raw.githubusercontent.com/mtxadmin/ublock/master/hosts.txt -O /tmp/hosts19
cat /tmp/hosts* > /tmp/hh1
cat hh1 | sort -u > hh2

exit 0
